from django.contrib import admin
from .models import Recursos, Datos, Comentarios, Votos
# Register your models here.
admin.site.register(Recursos)
admin.site.register(Datos)
admin.site.register(Comentarios)
admin.site.register(Votos)
