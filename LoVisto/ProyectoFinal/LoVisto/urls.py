from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('',views.pagina_principal),
    path('usuario/<str:llave>',views.pagina_usuario),
    path('informacion', views.pagina_informacion),
    path('aportaciones', views.pagina_aportaciones),
    path('<str:llave>',views.pagina_aportacion)
]