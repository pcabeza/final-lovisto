
from django.db import models

# Create your models here.

class Recursos(models.Model):

    RECURSOS=[
        ('WIK', 'wikipedia'),
        ('AEM','aemet'),
        ('RED', 'reddit'),
        ('YUT','youtube'),
        ('NR','other')]

    recurso = models.CharField(max_length=3, choices=RECURSOS)

    def __str__(self):
       return str(self.id) + ":" + self.recurso

class Datos(models.Model):

    noticia = models.CharField(max_length=64)
    url = models.TextField()
    usuario = models.TextField()

    megusta = models.IntegerField()
    nomegusta = models.IntegerField()

    ncomentarios = models.IntegerField()
    recurso = models.ManyToManyField(Recursos) #Mirar si hace falta
    fecha = models.DateTimeField('published')

    def __str__(self):
       return self.noticia + ":" + self.url

class Comentarios(models.Model):

    datos = models.ForeignKey(Datos,on_delete=models.CASCADE)
    titulo = models.CharField(max_length=150) #Mirar si hace falta
    comentario = models.TextField()
    url = models.TextField(default = "No hay imagen")
    usuario = models.TextField()
    fecha = models.DateTimeField('published')

    def __str__(self):
        return self.titulo + ":" + self.comentario

class Votos(models.Model):

    datos = models.ForeignKey(Datos,on_delete=models.CASCADE)
    usuario = models.TextField()
    valor = models.TextField()

    def __str__(self):
       return str(self.datos) + ":" + self.usuario

