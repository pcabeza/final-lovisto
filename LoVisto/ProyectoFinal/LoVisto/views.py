from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from .models import Datos, Votos, Comentarios
from django.contrib.auth import logout
from django.utils import timezone
import requests
from django.http import JsonResponse
from bs4 import BeautifulSoup
from django.http import HttpResponse
from django.template import loader

@csrf_exempt
def pagina_principal(request):
    modo=0
    if request.method == "POST":
        action = request.POST['action']

        if action == "Login":
            return redirect('login/')

        elif action == "Modo_Oscuro":
            modo = 1

        elif action == "Modo_Claro":
            modo= 0

        elif action == "Logout":
            logout(request)
            return redirect('/')

        elif action == "Insertar":
            noticia = request.POST['noticia']
            url = request.POST['url']

            try:
                c = Datos.objects.get(noticia=noticia)
                c.delete()

            except Datos.DoesNotExist:
                c = Datos(noticia=noticia,url=url,usuario=request.user.username,megusta=0,nomegusta=0,ncomentarios=0,fecha=timezone.now())
                c.save()

        elif action == "Datos Usuario":
            return redirect('/usuario/' + request.user.username)

        elif action == "Aportaciones":
            return redirect('/aportaciones')

        elif action == "Informacion":
            return redirect('/informacion')

    datos_list = Datos.objects.all()
    datos_list = list(reversed(datos_list))
    datos_list_10 = datos_list[0:10]
    datos_list_3 = datos_list[0:3]
    aportaciones = 10
    megusta = 0
    nomegusta = 0
    ncomentarios = 0
    modo = modo

    context = {
        'datos_list_10': datos_list_10,
        'datos_list_3': datos_list_3,
        'datos_list': datos_list,
        'apartaciones': aportaciones,
        'megusta': megusta,
        'nomegusta':nomegusta,
        'ncomentarios': ncomentarios,
        'modo':modo
    }
    return render(request, 'LoVisto/PaginaPrincipal.html', context)

def pagina_usuario(request,llave):

    modo = 0
    if request.method =="POST":
        action=request.POST['action']

        if action== "Logout":
            logout(request)
            return redirect('/')
        elif action == "Modo_Oscuro":
            modo = 1

        elif action == "Modo_Claro":
            modo= 0

        elif action == "Inicio":
            return redirect('/')

        elif action== "Informacion":
            return redirect('/informacion')

        elif action== "Aportaciones":
            return redirect('/aportaciones')

    datos_list = Datos.objects.all()
    comentarios_list = Comentarios.objects.all()
    votos_list = Votos.objects.all()
    datos_list_3 = datos_list[0:3]
    modo=modo

    context = {
         'datos_list': datos_list,
         'comentarios_list': comentarios_list,
         'votos_list':votos_list,
         'datos_list_3':datos_list_3,
         'modo':modo

     }
    return render(request,'LoVisto/PaginaUsuario.html',context)

def pagina_informacion(request):
    modo=0
    if request.method =="POST":
        action=request.POST['action']

        if action == "Inicio":
            return redirect('/')

        elif action == "Login":
            return redirect('login/')

        elif action == "Modo_Oscuro":
            modo = 1

        elif action == "Modo_Claro":
            modo= 0

        elif action== "Logout":
            logout(request)
            return redirect('/')

        elif action== "Informacion":
            return redirect('/informacion')

        elif action == "Datos Usuario":
            return redirect('/usuario/'+request.user.username)

        elif action== "Aportaciones":
            return redirect('/aportaciones')

    datos_list= Datos.objects.all()
    datos_list_3 = datos_list[0:3]
    modo=modo

    context={
        'datos_list_3': datos_list_3,
        'modo':modo
    }

    return render(request,'LoVisto/PaginaInformacion.html',context)


def pagina_aportaciones(request):
    modo=0
    if request.method == "POST":
        action = request.POST['action']

        if action == "Inicio":
            return redirect('/')

        elif action == "Logout":
            logout(request)
            return redirect('/')

        elif action == "Login":
            return redirect('login/')

        elif action == "Modo_Oscuro":
            modo = 1

        elif action == "Modo_Claro":
            modo= 0

        elif action == "Datos Usuario":
            return redirect('/usuario/' + request.user.username)

        elif action == "Informacion":
            return redirect('/informacion')

    datos_list = list(reversed(Datos.objects.all()))
    comentarios_list = Comentarios.objects.all()
    votos_list = Votos.objects.all()
    datos_list_3 = datos_list[0:3]
    modo=modo

    context = {
        'datos_list': datos_list,
        'comentarios_list': comentarios_list,
        'votos_list': votos_list,
        'datos_list_3': datos_list_3,
        'modo':modo
    }

    return render(request, 'LoVisto/PaginaAportaciones.html', context)

def pagina_aportacion(request,llave):
    modo=0
    if request.method =="PUT":
        valor = request.body.decode('utf-8')

    if request.method =="POST":
        action = request.POST['action']
        noticia = llave

        if action == "Inicio":
            return redirect('/')

        elif action == "Logout":
            logout(request)
            return redirect('/')

        elif action == "Login":
            return redirect('login/')

        elif action == "Modo_Oscuro":
            modo = 1

        elif action == "Modo_Claro":
            modo= 0

        elif action == "Datos Usuario":
            return redirect('/usuario/' + request.user.username)

        elif action == "Informacion":
            return redirect('/informacion')

        elif action== "Aportaciones":
            return redirect('/aportaciones')

        elif action == "Cambiar contenido":
            try:
                c= Datos.objects.get(noticia=llave)
                c.url=request.POST['valor']
                c.save()

            except Datos.DoesNotExist:
                c=Datos(noticia=noticia, url=request.POST['valor'])
                c.save()

        elif action == "Mg":
            datos = Datos.objects.get(noticia=noticia)
            datos.megusta= datos.megusta + 1
            datos.save()

            megusta=Votos(datos=Datos.objects.get(noticia=noticia),usuario=request.user.username,valor="Mg")
            megusta.save()

        elif action == "NoMg":
            datos= Datos.objects.get(noticia=noticia)
            datos.nomegusta= datos.nomegusta + 1
            datos.save()

            nomegusta = Votos(datos=Datos.objects.get(noticia=noticia),usuario=request.user.username,valor="NoMg")
            nomegusta.save()

        elif action == "Borrar contenido":
            try:
                c= Datos.objects.get(noticia=noticia)
                c.delete()
                return redirect('/')

            except Datos.DoesNotExist:
                c=Datos.objects.get(noticia=noticia, url=valor)
                c.save()

        elif action == "Borrar voto":
            datos = Datos.objects.get(noticia=llave)
            borrar_voto = Votos.objects.get(datos=Datos.objects.get(noticia=noticia),usuario=request.user.username)

            if borrar_voto.valor=="Mg":
                datos.megusta= datos.megusta - 1

            elif borrar_voto.valor=="NoMg":
                datos.nomegusta= datos.nomegusta - 1
            datos.save()
            borrar_voto.delete()

        elif action == "Enviar comentario":
            datos = Datos.objects.get(noticia=noticia)
            datos.ncomentarios= datos.ncomentarios + 1

            c = Comentarios(datos=datos, titulo=request.POST['titulo'],
                         comentario=request.POST['comentario'],
                         url= request.POST['url'],
                         fecha=timezone.now(),
                         usuario=request.user.username)
            c.save()
            datos.save()

        elif action == "Nueva Aportacion":
            try:
                c= Datos.objects.get(noticia=noticia)
                c.delete()

            except Datos.DoesNotExist:
                pass

            c=Datos(noticia=noticia, url=request.POST['valor'],usuario=request.user.username,megusta=0,nomegusta=0,ncomentarios=0,fecha=timezone.now())
            c.save()

    try:
        datos=Datos.objects.get(noticia=llave)
        context ={
            'datos': datos,
            'modo':modo
        }

    except Datos.DoesNotExist:
        context = {
            'llave': llave,
            'modo':modo
        }

        if request.GET.get("format") == "xml/":

            XML = loader.get_template('LoVisto/XML.html')
            datos_list = Datos.objects.all()
            contexto = {'datos_list': datos_list}

            return HttpResponse(XML.render(contexto, request))

        if request.GET.get("format") == "json/":

            datos = Datos.objects.filter().values()

            return JsonResponse({"datos_list": list(datos)})
        return render(request, 'LoVisto/RecursoNoEncontrado.html', context)

    datos_list = Datos.objects.all()
    datos_list_3 = datos_list[0:3]
    llave = llave
    comentarios_list = Comentarios.objects.all()
    votos_list = Votos.objects.all()

    datos = Datos.objects.get(noticia=llave)
    url = datos.url

    headers = {'User-Agent': 'Mozilla/5.0'}

    try:
        page = requests.get(url, headers=headers)
        soup = BeautifulSoup(page.text, 'html.parser')

        if "www.reddit.com" in url:
            try:
                text = soup.find("p", class_="_1qeIAgB0cPwnLhDF9XSiJM").text

                # aprobacion = soup.find("div", class_="t4Hq30BDzTeJ85vREX7_M").text
                title = soup.find("title").text

                context = {
                    'datos': datos,
                    'comentarios_list': comentarios_list,
                    'votos_list': votos_list,
                    'texto': text,
                    'title': title,
                    'datos_list_3': datos_list_3,
                    'modo':modo
                }

                return render(request, 'LoVisto/PaginaReddit.html', context)

            except AttributeError:

                context = {
                    'datos': datos,
                    'comentarios_list': comentarios_list,
                    'votos_list': votos_list,
                    'datos_list_3': datos_list_3,
                    'modo': modo
                }
                return render(request, 'LoVisto/PaginaError.html', context)

        elif "es.wikipedia.org" in url:
            try:
                images = soup.findAll('img')
                title = soup.find(id="firstHeading").text
                text = soup.find(id="bodyContent").text
                texto = text[0:400]
                imagen = ((images[0])['src'])

                context = {
                    'datos': datos,
                    'datos_list': datos_list,
                    'votos_list': votos_list,
                    'title': title,
                    'texto':texto,
                    'imagen': imagen,
                    'datos_list_3': datos_list_3,
                    'modo': modo
                }
                return render(request, 'LoVisto/PaginaWikipedia.html', context)

            except AttributeError:
                context = {
                    'datos': datos,
                    'datos_list': datos_list,
                    'votos_list': votos_list,
                    'datos_list_3': datos_list_3,
                    'modo': modo
                }
                return render(request, 'LoVisto/PaginaError.html', context)

        elif "www.aemet.es" in url:
            try:
                title = soup.find("title").text
                temperatura = soup.find("div", class_="no_wrap").text
                fecha = soup.find("th", class_="borde_izq_dcha_fecha").text
                maxymin = soup.find("td", class_="alinear_texto_centro no_wrap comunes").text
                precipitacion = soup.find("td", class_="nocomunes").text

                context = {
                    'datos': datos,
                    'comentarios_list': comentarios_list,
                    'votos_list': votos_list,
                    'title': title,
                    'temperatura': temperatura,
                    'fecha': fecha,
                    'maxymin': maxymin,
                    'precipitacion': precipitacion,
                    'datos_list_3': datos_list_3,
                    'modo': modo
                }

                return render(request, 'LoVisto/PaginaAEMET.html', context)

            except AttributeError:

                context = {
                    'datos': datos,
                    'comentarios_list': comentarios_list,
                    'votos_list': votos_list,
                    'datos_list_3': datos_list_3,
                    'modo': modo
                }

                return render(request, 'LoVisto/PaginaError.html', context)

        elif "elpais.com" in url:

            try:
                title = soup.find("h1", class_="a_t").text
                texto = soup.find("h2", class_="a_st").text
                imagen = soup.find("img", class_= "block")['src']

                context = {
                    'datos': datos,
                    'datos_list': datos_list,
                    'votos_list': votos_list,
                    'title': title,
                     'texto':texto,
                    'imagen': imagen,
                    'datos_list_3': datos_list_3,
                    'modo': modo
                }

                return render(request, 'LoVisto/PaginaElPais.html', context)

            except AttributeError:

                context = {
                    'datos': datos,
                    'comentarios_list': comentarios_list,
                    'votos_list': votos_list,
                    'datos_list_3': datos_list_3,
                    'modo': modo
                }

            return render(request, 'LoVisto/PaginaError.html', context)

        elif "www.elmundo.es" in url:
            try:
                title = soup.find("h1").text

                texto = soup.find("p", class_="ue-c-article__standfirst").text

                imagen = soup.find("img", class_="ue-c-article__image")['src']

                context = {
                    'datos': datos,
                    'datos_list': datos_list,
                    'votos_list': votos_list,
                    'title': title,
                    'texto': texto,
                    'imagen': imagen,
                    'datos_list_3': datos_list_3,
                    'modo': modo
                }

                return render(request, 'LoVisto/PaginaElMundo.html', context)

            except AttributeError:

                context = {
                    'datos': datos,
                    'comentarios_list': comentarios_list,
                    'votos_list': votos_list,
                    'datos_list_3': datos_list_3,
                    'modo': modo
                }
                return render(request, 'LoVisto/PaginaError.html', context)

        else:

            try:
                images = soup.findAll('img')
                title = soup.find("title").text
                imagen = ((images[1])['src'])
                context = {
                    'datos': datos,
                    'comentarios_list': comentarios_list,
                    'votos_list': votos_list,
                    'title': title,
                    'imagen': imagen,
                    'datos_list_3': datos_list_3,
                    'modo': modo
                }
                return render(request, 'LoVisto/PaginaNoReconocido.html', context)

            except AttributeError:

                context = {
                    'datos': datos,
                    'comentarios_list': comentarios_list,
                    'votos_list': votos_list,
                    'datos_list_3': datos_list_3,
                    'modo': modo
                }

            return render(request, 'LoVisto/PaginaError.html', context)
    except:
        context = {
            'datos': datos,
            'comentarios_list': comentarios_list,
            'votos_list': votos_list,
            'datos_list_3': datos_list_3,
            'modo': modo
        }

        return render(request, 'LoVisto/PaginaError.html', context)
