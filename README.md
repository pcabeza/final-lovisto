# Final Lo Visto

Práctica final del curso 2020/21
# Entrega practica
## Datos
* Nombre:Pablo Cabeza Portalo (pcabeza- nombre laboratorio)
* Titulación: Tecnologías de Telecomunicación
* Despliegue (url):http://pcabeza.pythonanywhere.com/
* Video básico (url):https://youtu.be/a_jkCKOt8TM
* Video parte opcional (url):https://youtu.be/efr6MTzo9qY
## Cuenta Admin Site
* user1/user1 - user2/user2(los usuarios tanto de la aplicacion como el admin,coinciden debido a que han sido creados mediante "makesuperuser").
## Cuentas usuarios
* user1/user1 
* user2/user2
* ...
## Resumen parte obligatoria
* Esta parte ha sido implementada de forma satisfactoria en la mayor parte de sus ámbitos. Youtube no pude parsearlo ya que no supe como hacerlo porque me pedia un registro previo en la página. Para compensar esta aportación, he implementado la de "El Mundo" y "El Pais" como parte opcional de la misma.
## Lista partes opcionales
* Nombre parte: Inclusión de un favicon del sitio.
* Nombre parte:Incorporación de datos de otros tipos de recurso además de los obligatorios.
* Nombre parte: Inclusión de imágenes (no solo texto) en los comentarios.
* Nombre parte:El usuario que ha realizado la aportación podrá cambiar el contenido de la misma.
* Nombre parte: Previsualización de las diferentes aportaciones en su pagina de aportación correspondiente.

